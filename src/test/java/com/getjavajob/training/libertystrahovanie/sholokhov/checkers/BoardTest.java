package com.getjavajob.training.libertystrahovanie.sholokhov.checkers;

import com.getjavajob.training.libertystrahovanie.sholokhov.checkers.enums.PieceColor;
import com.getjavajob.training.libertystrahovanie.sholokhov.checkers.models.Board;
import com.getjavajob.training.libertystrahovanie.sholokhov.checkers.models.Checker;
import com.getjavajob.training.libertystrahovanie.sholokhov.checkers.models.Coordinates;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by steax on 16.05.2016.
 */
public class BoardTest {
    private Board board;

    @Before
    public void createField() throws Exception {
        board = new Board();
    }

    @Test
    public void movePieceCorrectTest() throws Exception {
        Coordinates from = new Coordinates(6, 0);
        Coordinates to = new Coordinates(7, 1);
        board.getField()[7][1] = null;
        board.getField()[5][1] = null;
        board.getField()[6][0] = new Checker(from, PieceColor.WHITE, board.getField());
        boolean moveIsDone = board.movePiece(from, to);
        Assert.assertTrue(moveIsDone);
    }

    @Test
    public void movePieceIncorrectTest() throws Exception {
        Coordinates from = new Coordinates(6, 0);
        Coordinates to = new Coordinates(7, 1);
        board.getField()[6][0] = new Checker(from, PieceColor.WHITE, board.getField());
        boolean moveIsDone = board.movePiece(from, to);
        Assert.assertFalse(moveIsDone);
    }

}