package com.getjavajob.training.libertystrahovanie.sholokhov.checkers;

import com.getjavajob.training.libertystrahovanie.sholokhov.checkers.enums.PieceColor;
import com.getjavajob.training.libertystrahovanie.sholokhov.checkers.models.Board;
import com.getjavajob.training.libertystrahovanie.sholokhov.checkers.models.Checker;
import com.getjavajob.training.libertystrahovanie.sholokhov.checkers.models.Coordinates;
import com.getjavajob.training.libertystrahovanie.sholokhov.checkers.models.Piece;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by steax on 16.05.2016.
 */
public class CheckerTest {
    private Piece[][] expectedField;
    private Board board;

    @Before
    public void createField() throws Exception {
        expectedField = new Piece[8][8];
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                if ((i + j) % 2 == 0 && i < 3) {
                    expectedField[i][j] = new Checker(new Coordinates(i, j), PieceColor.WHITE, expectedField);
                } else if ((i + j) % 2 == 0 && i > 4) {
                    expectedField[i][j] = new Checker(new Coordinates(i, j), PieceColor.BLACK, expectedField);
                }
            }
        }
        board = new Board();
    }

    @Test
    public void moveCorrectTest() throws Exception {
        Coordinates from = new Coordinates(2, 0);
        Coordinates to = new Coordinates(3, 1);
        boolean moveIsDone = board.movePiece(from, to);
        expectedField[to.getX()][to.getY()] = expectedField[from.getX()][from.getY()];
        expectedField[to.getX()][to.getY()].setCoordinates(to);
        expectedField[from.getX()][from.getY()] = null;
        Assert.assertArrayEquals(expectedField, board.getField());
        Assert.assertTrue(moveIsDone);
    }

    @Test
    public void moveIncorrectTest() throws Exception {
        Coordinates from = new Coordinates(2, 0);
        Coordinates to = new Coordinates(4, 2);
        boolean moveIsDone = board.movePiece(from, to);
        Assert.assertArrayEquals(expectedField, board.getField());
        Assert.assertFalse(moveIsDone);
    }

    @Test
    public void moveCapturedTest() throws Exception {
        //устанавливаем черную шашку рядом с белыми
        board.getField()[3][1] = new Checker(new Coordinates(3, 1), PieceColor.BLACK, board.getField());
        Coordinates from = new Coordinates(2, 0);
        Coordinates to = new Coordinates(4, 2);
        //рубим
        boolean moveIsDone = board.movePiece(from, to);
        Assert.assertTrue(moveIsDone);
    }

    @Test
    public void getForcedMoveTest() throws Exception {
        //устанавливаем черную шашку рядом с белыми
        board.getField()[3][1] = new Checker(new Coordinates(3, 1), PieceColor.BLACK, board.getField());
        expectedField[3][1] = new Checker(new Coordinates(3, 1), PieceColor.BLACK, expectedField);
        Coordinates from = new Coordinates(2, 2);
        Coordinates to = new Coordinates(3, 3);
        //пробуем совершить ход не срубив противника
        boolean moveIsDone = board.movePiece(from, to);
        Assert.assertArrayEquals(expectedField, board.getField());
        Assert.assertFalse(moveIsDone);
    }
}