package com.getjavajob.training.libertystrahovanie.sholokhov.checkers.models;

import com.getjavajob.training.libertystrahovanie.sholokhov.checkers.enums.PieceColor;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by steax on 15.05.2016.
 */
public class King extends Piece {

    public King(Coordinates coordinates, PieceColor color, Piece[][] field) {
        super(coordinates, color, field);
    }

    @Override
    public boolean move(Coordinates to) {
        int fromX = coordinates.getX();
        int fromY = coordinates.getY();
        int toX = to.getX();
        int toY = to.getY();
        Set<Coordinates> forcedMoves = getForcedMove();
        Set<Coordinates> possibleMoves = getKingPossibleMoves();
        if (forcedMoves.isEmpty() &&
                possibleMoves.contains(to) &&
                new Checker(null, null, field).getForcedMove().isEmpty() &&
                field[fromX][fromY].isKing() &&
                field[fromX][fromY].color == this.color) {
            field[toX][toY] = field[fromX][fromY];
            field[toX][toY].setCoordinates(to);
            field[fromX][fromY] = null;
            return true;
        } else if (forcedMoves.contains(to) &&
                field[fromX][fromY].isKing() &&
                field[fromX][fromY].color == this.color) {
            eatCheckers(to);
            forcedMoves.remove(to);
            field[toX][toY] = field[fromX][fromY];
            field[toX][toY].setCoordinates(to);
            field[fromX][fromY] = null;
            return forcedMoves.isEmpty();
        }
        return false;
    }

    @Override
    protected Set<Coordinates> getForcedMove() {
        Set<Coordinates> forcedMoves = new HashSet<>();
        PieceColor opponentColor = this.color == PieceColor.BLACK ? PieceColor.WHITE : PieceColor.BLACK;
        for (int i = 0; i < field.length; i++) {
            for (int j = 0; j < field.length; j++) {
                if (field[i][j] != null &&
                        field[i][j].isKing() &&
                        field[i][j].color == this.color) {
                    int x = i;
                    int y = j;
                    while (x != 7 && y != 7) {
                        if ((field[x][y] != null &&
                                field[x][y].color == opponentColor) &&
                                field[x + 1][y + 1] == null) {
                            forcedMoves.add(new Coordinates(x + 1, y + 1));
                        }
                        x++;
                        y++;
                    }
                    x = i;
                    y = j;
                    while (x != 7 && y != 0) {
                        if ((field[x][y] != null &&
                                field[x][y].color == opponentColor) &&
                                field[x + 1][y - 1] == null) {
                            forcedMoves.add(new Coordinates(x + 1, y - 1));
                        }
                        x++;
                        y--;
                    }
                    x = i;
                    y = j;
                    while (x != 0 && y != 7) {
                        if ((field[x][y] != null &&
                                field[x][y].color == opponentColor) &&
                                field[x - 1][y + 1] == null) {
                            forcedMoves.add(new Coordinates(x - 1, y + 1));
                        }
                        x--;
                        y++;
                    }
                    x = i;
                    y = j;
                    while (x != 0 && y != 0) {
                        if ((field[x][y] != null &&
                                field[x][y].color == opponentColor) &&
                                field[x - 1][y - 1] == null) {
                            forcedMoves.add(new Coordinates(x - 1, y - 1));
                        }
                        x--;
                        y--;
                    }
                }
            }
        }
        return forcedMoves;
    }

    private Set<Coordinates> getKingPossibleMoves() {
        Set<Coordinates> possibleMoves = new HashSet<>();
        int x = coordinates.getX();
        int y = coordinates.getY();
        while (x != 7 && y != 7) {
            if (field[x][y] == null) {
                possibleMoves.add(new Coordinates(x, y));
            }
            x++;
            y++;
        }

        x = coordinates.getX();
        y = coordinates.getY();
        while (x != 0 && y != 7) {
            if (field[x][y] == null) {
                possibleMoves.add(new Coordinates(x, y));
            }
            x--;
            y++;
        }

        x = coordinates.getX();
        y = coordinates.getY();
        while (x != 0 && y != 0) {
            if (field[x][y] == null) {
                possibleMoves.add(new Coordinates(x, y));
            }
            x--;
            y--;
        }

        x = coordinates.getX();
        y = coordinates.getY();
        while (x != 7 && y != 0) {
            if (field[x][y] == null) {
                possibleMoves.add(new Coordinates(x, y));
            }
            x++;
            y--;
        }
        return possibleMoves;
    }

    private void eatCheckers(Coordinates to) {
        int fromX = coordinates.getX();
        int fromY = coordinates.getY();
        int toX = to.getX();
        int toY = to.getY();
        PieceColor opponentColor = this.color == PieceColor.BLACK ? PieceColor.WHITE : PieceColor.BLACK;
        if (fromX < toX && fromY < toY) {
            while (fromX != toX && fromY != toY) {
                if (field[fromX][fromY] != null &&
                        field[fromX][fromY].color == opponentColor) {
                    field[fromX][fromY] = null;
                }
                fromX++;
                fromY++;
            }
        } else if (fromX > toX && fromY > toY) {
            while (fromX != toX && fromY != toY) {
                if (field[fromX][fromY] != null &&
                        field[fromX][fromY].color == opponentColor) {
                    field[fromX][fromY] = null;
                }
                fromX--;
                fromY--;
            }
        } else if (fromX < toX && fromY > toY) {
            while (fromX != toX && fromY != toY) {
                if (field[fromX][fromY] != null &&
                        field[fromX][fromY].color == opponentColor) {
                    field[fromX][fromY] = null;
                }
                fromX++;
                fromY--;
            }
        } else if (fromX > toX && fromY < toY) {
            while (fromX != toX && fromY != toY) {
                if (field[fromX][fromY] != null &&
                        field[fromX][fromY].color == opponentColor) {
                    field[fromX][fromY] = null;
                }
                fromX--;
                fromY++;
            }
        }
    }

    @Override
    public boolean isKing() {
        return true;
    }
}
