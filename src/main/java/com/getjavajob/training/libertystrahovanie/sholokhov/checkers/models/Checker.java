package com.getjavajob.training.libertystrahovanie.sholokhov.checkers.models;

import com.getjavajob.training.libertystrahovanie.sholokhov.checkers.enums.PieceColor;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by steax on 15.05.2016.
 */
public class Checker extends Piece {

    public Checker(Coordinates coordinates, PieceColor color, Piece[][] field) {
        super(coordinates, color, field);
    }

    @Override
    public boolean move(Coordinates to) {
        int fromX = coordinates.getX();
        int fromY = coordinates.getY();
        int toX = to.getX();
        int toY = to.getY();
        PieceColor opponentColor = this.color == PieceColor.BLACK ? PieceColor.WHITE : PieceColor.BLACK;
        if (Math.abs(toX - fromX) == 2 && Math.abs(toY - fromY) == 2) {
            int capturedX = (fromX + toX) / 2;
            int capturedY = (fromY + toY) / 2;
            Piece captured = field[capturedX][capturedY];
            if ((captured!=null && captured.color == opponentColor) &&
                    field[fromX][fromY].color == this.color &&
                    field[toX][toY] == null) {
                field[toX][toY] = field[fromX][fromY];
                field[toX][toY].setCoordinates(new Coordinates(toX, toY));
                field[fromX][fromY] = null;
                field[capturedX][capturedY] = null;
                return getForcedMove().isEmpty();
            }
        }
        if ((this.color == PieceColor.WHITE && fromX > toX) ||
                (this.color == PieceColor.BLACK && toX > fromX)) {
            return false;
        } else if (new King(null, null, field).getForcedMove().isEmpty() &&
                getForcedMove().isEmpty() &&
                field[fromX][fromY].color == this.color &&
                field[toX][toY] == null &&
                (fromX - toX == 1 || fromX - toX == -1) &&
                (fromY - toY == 1 || fromY - toY == -1)) {
            field[toX][toY] = field[fromX][fromY];
            field[toX][toY].setCoordinates(new Coordinates(toX, toY));
            field[fromX][fromY] = null;
            return true;
        }
        return false;
    }

    @Override
    protected Set<Coordinates> getForcedMove() {
        Set<Coordinates> forcedMoves = new HashSet<>();
        PieceColor opponentColor = this.color == PieceColor.BLACK ? PieceColor.WHITE : PieceColor.BLACK;
        for (int i = 0; i < field.length; i++) {
            for (int j = 0; j < field.length; j++) {
                try {
                    if (field[i][j] != null && field[i][j].color == this.color &&
                            (((field[i + 1][j + 1] != null && field[i + 1][j + 1].color == opponentColor) &&
                                    field[i + 2][j + 2] == null) ||
                                    ((field[i + 1][j - 1] != null && field[i + 1][j - 1].color == opponentColor) &&
                                            field[i + 2][j - 2] == null) ||
                                    ((field[i - 1][j + 1] != null && field[i - 1][j + 1].color == opponentColor) &&
                                            field[i - 2][j + 2] == null) ||
                                    ((field[i - 1][j - 1] != null && field[i - 1][j - 1].color == opponentColor) &&
                                            field[i - 2][j - 2] == null))) {
                        forcedMoves.add(new Coordinates(i, j));
                    }
                } catch (ArrayIndexOutOfBoundsException ignored) {
                    //todo переделать так, чтобы не было выхода за пределы массива
                }
            }
        }
        return forcedMoves;
    }

    @Override
    public boolean isKing() {
        return false;
    }
}
