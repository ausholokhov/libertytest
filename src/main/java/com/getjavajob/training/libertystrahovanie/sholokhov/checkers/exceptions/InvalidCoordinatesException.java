package com.getjavajob.training.libertystrahovanie.sholokhov.checkers.exceptions;

/**
 * Created by steax on 15.05.2016.
 */
public class InvalidCoordinatesException extends Exception {

    public InvalidCoordinatesException() {
    }

    public InvalidCoordinatesException(String message) {
        super(message);
    }
}
