package com.getjavajob.training.libertystrahovanie.sholokhov.checkers.game;

import com.getjavajob.training.libertystrahovanie.sholokhov.checkers.enums.PieceColor;
import com.getjavajob.training.libertystrahovanie.sholokhov.checkers.enums.Player;
import com.getjavajob.training.libertystrahovanie.sholokhov.checkers.exceptions.InvalidCoordinatesException;
import com.getjavajob.training.libertystrahovanie.sholokhov.checkers.models.Board;
import com.getjavajob.training.libertystrahovanie.sholokhov.checkers.models.Coordinates;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by steax on 15.05.2016.
 */
public class Game {

    private Player currentPlayer = Player.WHITE;
    private Board board = new Board();
    private BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    public void changeCurrentPlayer() {
        currentPlayer = currentPlayer == Player.WHITE ? Player.BLACK : Player.WHITE;
    }

    private boolean gameComplete() {
        int whiteCheckersCounter = 0;
        int blackCheckersCounter = 0;
        for (int i = 0; i < board.getField().length; i++) {
            for (int j = 0; j < board.getField().length; j++) {
                if (board.getField()[i][j] != null &&
                        board.getField()[i][j].getColor() == PieceColor.BLACK) {
                    blackCheckersCounter++;
                } else if (board.getField()[i][j] != null &&
                        board.getField()[i][j].getColor() == PieceColor.WHITE) {
                    whiteCheckersCounter++;
                }
            }
        }
        return whiteCheckersCounter == 0 || blackCheckersCounter == 0;
    }

    private Player getWinner() {
        return currentPlayer == Player.BLACK ? Player.WHITE : Player.BLACK;
    }

    private void readMove() throws IOException, NumberFormatException, InvalidCoordinatesException {
        System.out.println(currentPlayer + " ваш ход!");
        System.out.print("Y: ");
        int x = Integer.parseInt(reader.readLine()) - 1;
        System.out.print("X: ");
        int y = Integer.parseInt(reader.readLine()) - 1;
        System.out.print("новая Y: ");
        int newX = Integer.parseInt(reader.readLine()) - 1;
        System.out.print("новая X: ");
        int newY = Integer.parseInt(reader.readLine()) - 1;
        if (x > 7 || x < 0 || newX > 7 || newX < 0 || y > 7 || y < 0 || newY > 7 || newY < 0) {
            throw new InvalidCoordinatesException();
        } else if (board.movePiece(new Coordinates(x, y), new Coordinates(newX, newY))) {
            changeCurrentPlayer();
        } else {
            System.out.println("Не правильный ход, попробуйте снова!");
        }
    }

    public void start() {
        board.printField();
        System.out.println("Правила игры: \n" +
                "1) Шашки ходят по диагонали на 1 клетку вперед.\n" +
                "2) Шашки рубят по диагонали в любую сторону. \n" +
                "3) Дамка ходит на любое свободное количество клеток по диагонали. \n" +
                "4) Дамка ходит и рубит по диагонали в любую сторону \n" +
                "5) Рубить обязательно!\n");
        System.out.println("Для осуществления хода нужно ввести текущие и новые координаты шашки, " +
                "сначала по оси Y потом по оси X");

        while (!gameComplete()) {
            try {
                readMove();
                board.printField();
            } catch (IOException | InvalidCoordinatesException | NumberFormatException e) {
                System.out.println("Введены не корректные данные");
            }
        }
        System.out.println(getWinner() + " победил!");
    }
}
