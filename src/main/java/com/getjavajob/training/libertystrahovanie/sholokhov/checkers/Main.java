package com.getjavajob.training.libertystrahovanie.sholokhov.checkers;

import com.getjavajob.training.libertystrahovanie.sholokhov.checkers.game.Game;

public class Main {

    public static void main(String[] args) {
        new Game().start();
    }
}
