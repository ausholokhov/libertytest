package com.getjavajob.training.libertystrahovanie.sholokhov.checkers.models;

import com.getjavajob.training.libertystrahovanie.sholokhov.checkers.enums.PieceColor;

import java.util.Set;

/**
 * Created by steax on 15.05.2016.
 */
public abstract class Piece {
    protected Coordinates coordinates;
    protected PieceColor color;
    protected Piece[][] field;

    public Piece(Coordinates coordinates, PieceColor color, Piece[][] field) {
        this.coordinates = coordinates;
        this.color = color;
        this.field = field;
    }

    public Coordinates getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(Coordinates coordinates) {
        this.coordinates = coordinates;
    }

    public PieceColor getColor() {
        return color;
    }

    public void setColor(PieceColor color) {
        this.color = color;
    }

    public abstract boolean isKing();

    public abstract boolean move(Coordinates to);

    protected abstract Set<Coordinates> getForcedMove();

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Piece piece = (Piece) o;
        return coordinates != null ? coordinates.equals(piece.coordinates) : piece.coordinates == null && color == piece.color;

    }

    @Override
    public int hashCode() {
        int result = coordinates != null ? coordinates.hashCode() : 0;
        result = 31 * result + (color != null ? color.hashCode() : 0);
        return result;
    }
}
