package com.getjavajob.training.libertystrahovanie.sholokhov.checkers.models;

import com.getjavajob.training.libertystrahovanie.sholokhov.checkers.enums.PieceColor;

/**
 * Created by steax on 15.05.2016.
 */
public class Board {

    private Piece[][] field;

    public Board() {
        field = new Piece[8][8];
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                if ((i + j) % 2 == 0 && i < 3) {
                    field[i][j] = new Checker(new Coordinates(i, j), PieceColor.WHITE, field);
                } else if ((i + j) % 2 == 0 && i > 4) {
                    field[i][j] = new Checker(new Coordinates(i, j), PieceColor.BLACK, field);
                }
            }
        }
    }

    public Piece[][] getField() {
        return field;
    }

    public void printField() {
        StringBuilder drawField = new StringBuilder();
        drawField.append("  1   | 2  | 3  | 4  | 5  | 6  | 7  | 8  \n");
        drawField.append("-----------------------------------------\n");
        for (int i = 0; i < field.length; i++) {
            drawField.append(i + 1);
            for (int j = 0; j < field.length; j++) {
                drawField.append(drawPiece(field[i][j])).append(" ");
            }
            drawField.append("\n-----------------------------------------\n");
        }
        System.out.println(drawField);
    }

    private String drawPiece(Piece piece) {
        if (piece == null) {
            return "|   ";
        }
        if (piece.isKing() && piece.color == PieceColor.WHITE) {
            return "| W ";
        }
        if (piece.isKing() && piece.color == PieceColor.BLACK) {
            return "| B ";
        }
        if (!piece.isKing() && piece.color == PieceColor.WHITE) {
            return "| w ";
        }
        if (!piece.isKing() && piece.color == PieceColor.BLACK) {
            return "| b ";
        }
        return "|   ";
    }

    public boolean movePiece(Coordinates from, Coordinates to) {
        if (field[from.getX()][from.getY()] != null) {
            return field[from.getX()][from.getY()].move(to);
        }
        setKings();
        return true;
    }

    private void setKings() {
        for (int x = 0; x < field.length; x++) {
            for (int y = 0; y < field.length; y++) {
                if (field[x][y] != null &&
                        field[x][y].color == PieceColor.WHITE && x == 7) {
                    field[x][y] = new King(new Coordinates(x, y), PieceColor.WHITE, field);
                }
                if (field[x][y] != null &&
                        field[x][y].color == PieceColor.BLACK && x == 0) {
                    field[x][y] = new King(new Coordinates(x, y), PieceColor.BLACK, field);
                }
            }
        }
    }
}
